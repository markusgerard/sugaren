package model;

import java.util.ArrayList;
import java.util.Random;

import control.EstimatorInterface;

public class DummyLocalizer implements EstimatorInterface {
		
	private int rows, cols, head;
	private double[][] tMatrix;
	private int states;
	private ArrayList<double[]> obsDiagonals;
	private int[] trueLocation;
	private int trueState;
	private double[][] fVector;
	private double[][] tTranspose;
	private int[] currentReading;
	private double[] adjFvector;
	private int[] estimatePos;
	private ArrayList<Double> distances;

	public DummyLocalizer( int rows, int cols, int head) {
		this.rows = rows;
		this.cols = cols;
		this.head = head;
		states = rows*cols*head;
		tMatrix = new double[states][states];
		fillTmatrix();
		obsDiagonals = new ArrayList<double[]>();
		fillObsDiagonals();
		trueLocation = new int[2];
		Random rand = new Random();
		trueState = rand.nextInt(256) + 1;
		//trueState = 2;
		trueLocation[0] = stateInfo(trueState)[0];
		trueLocation[1] = stateInfo(trueState)[1];
		fVector = new double[states][1];
		adjFvector = new double[rows*cols];
		for(int i =  0; i<states;i++) {
			fVector[i][0] = 1.0/states;
			if(i%head == 0) {
			adjFvector[i/head] = (head*1.0)/states;
			}
		}
		//System.out.println(trueState + ": " + trueLocation[0] + ", " + trueLocation[1]);
		//testMatrixMul();
		tTranspose = getTranspose(tMatrix);
		currentReading = new int[2];
		estimatePos = new int[2];
		distances = new ArrayList<Double>();
	}	
	
	public int getNumRows() {
		return rows;
	}
	
	public int getNumCols() {
		return cols;
	}
	
	public int getNumHead() {
		return head;
	}
	
	public double getTProb( int x, int y, int h, int nX, int nY, int nH) {
		int currentState = coordToState(x,y,h);
		int newState = coordToState(nX,nY,nH);
		//System.out.println(x + ", " + y +  ", " + h + "(" + nX +  ", " + nY + ", " + nH + ")");
		return tMatrix[currentState-1][newState-1];
	}



	public double getOrXY( int rX, int rY, int x, int y, int h) {
		//sannolikheten att tillst�ndet x,y,h ger l�sningen att sugaren �r i rx,ry
		int index = rX*cols + rY;
		int state = coordToState(x,y,h);
		//System.out.println(index + ", " + state);
		//System.out.println(obsDiagonals.get(index)[state-1]);
		if(rX == -1 || rY == -1) {
			return obsDiagonals.get(rows*cols)[state-1];
		}
		return obsDiagonals.get(index)[state-1];
	}


	public int[] getCurrentTruePosition() {
		return trueLocation;
	}

	public int[] getCurrentReading() {
		return currentReading;
	}


	public double getCurrentProb( int x, int y) {
		int index = x*cols + y;
		//System.out.println(index);
		double sum = 0.0;
		
		//System.out.println("x: " + x + ", y: " + y);
		for(int i = 0; i < head;i++) {
			sum+= fVector[(index*4)+i][0];
		}
		//System.out.println("4-sum: " + sum);
		

	
		//System.out.println("SUM: " + sum);
		double ret = sum ;
		return adjFvector[index];
		//return ret;
	}
	
	public void update() {
		boolean isFacingWall = facingWall(trueState);
		int[] coords = stateInfo(trueState);
		int x = coords[0];
		int y = coords[1];
		int h = coords[2];
		ArrayList<Integer> possibleStatesList = getPossibleStates(trueState);
		//System.out.println(x + "," + y+ "," + h);
	//	System.out.println("nuv. true loc: " + trueLocation[0] + ", " + trueLocation[1] + " heading: " + stateInfo(trueState)[2]);
		int rand = new Random().nextInt(100);
		if(!isFacingWall && rand<70) {
			if(h == 0) {
				trueState = coordToState(x-1, y, h);
				trueLocation[0] = x-1;
				trueLocation[1] = y;
			}
			if(h == 1) {
				trueState = coordToState(x, y+1, h);
				trueLocation[0] = x;
				trueLocation[1] = y+1;
			}
			if(h == 2) {
				trueState = coordToState(x+1, y, h);
				trueLocation[0] = x+1;
				trueLocation[1] = y;
			}
			if(h == 3) {
				trueState = coordToState(x, y-1, h);
				trueLocation[0] = x;
				trueLocation[1] = y-1;
			}
		}
		if((!isFacingWall && rand >= 70) || isFacingWall) {
			int posState = possibleHeadings(trueState) - 1;
			//System.out.println(posState + "pos states");
			//System.out.println("pos state listsize: " + possibleStatesList.size());
			int newRand = new Random().nextInt(posState);
			//System.out.println("randnumber is; " + newRand);
			for(int i = 0; i < posState;i++) {
				if(newRand == i) {
					trueState = possibleStatesList.get(i);
					trueLocation[0] = stateInfo(trueState)[0];
					trueLocation[1] = stateInfo(trueState)[1];
				}
			}	
		}
		updateReading();
		calculateF();
		double dist = distance(trueLocation, estimatePos);
		distances.add(dist);
		double distSum100 = 0.0;
		if(distances.size() >= 100) {
			for(int i = distances.size() - 100; i < distances.size();i++) {
				distSum100 += distances.get(i);
			}
			double distAvg100 = distSum100/100.0;
			System.out.println("Update nbr: " + distances.size() + ". Avg distance of 100 latest updates: " + distAvg100);
		}
		//System.out.println("ny true loc: " + trueLocation[0] + ", " + trueLocation[1] + " heading: " + stateInfo(trueState)[2]);
	}
	
	private ArrayList<Integer> getPossibleStates(int state) {
		ArrayList<Integer> pStates = new ArrayList<Integer>();
		for(int i = 0; i < states;i++) {
			if(tMatrix[state-1][i] > 0 && tMatrix[state-1][i] < 0.7) {
				pStates.add(i+1);
			}
		}
		return pStates;
	}

	private int coordToState(int x, int y, int h) {
		int s = x*(cols*head) + y*head + (h+1);
		return s;
	}
	
	private void fillTmatrix() {
		for(int i = 1; i <= states; i++) {
			for(int j = 1; j <= states; j++) {
				if(possibleState(i,j)) {
					double ph = possibleHeadings(i);
					if(facingWall(i)) {
						tMatrix[i-1][j-1] = 1/ph; //next head is then randomly chosen so probability becomes 1/nbrOfpossibleMoves
					}
					else if(stateInfo(i)[2] == stateInfo(j)[2]) {
						tMatrix[i-1][j-1] = 0.7;
					}
					else {
						tMatrix[i-1][j-1] = 0.3 * (1/(ph-1));
					}
				}
				else {
					tMatrix[i-1][j-1] = 0.0;
				}
			}
		}
	}
	
	private boolean facingWall(int i) {
		int[] coords = stateInfo(i);
		int x = coords[0];
		int y = coords[1];
		int h = coords[2];
		if(h == 0 && x == 0) {
			return true;
		}
		if(h == 1 && y == (cols - 1)) {
			return true;
		}
		if(h == 2 && x == (rows - 1)) {
			return true;
		}
		if(h == 3 && y == 0) {
			return true;
		}
		return false;
	}

	
	private int possibleHeadings(int i) {
		int[] coords = stateInfo(i);
		int x = coords[0];
		int y = coords[1];
		//in corner
		if(x == 0 && y == 0 || x == 0 && y == (cols-1) ||
				x == (rows-1) && y == 0 || x == (rows-1) && y == (cols-1)) {
			return 2;
		}
		//next to one wall
		if(x == 0 || x == (rows-1) || y == 0 || y == (cols-1)) {
			return 3;
		}
		//neither
		return 4;
	}

	private boolean possibleState(int current, int next) {
		int[] currentCoord = stateInfo(current);
		int cx = currentCoord[0];
		int cy = currentCoord[1];
		int ch = currentCoord[2];
		int[] nextCoord = stateInfo(next);
		int nx = nextCoord[0];
		int ny = nextCoord[1];
		int nh = nextCoord[2];
		
		//checks if next move is a rotation to face wall
		if(cx == nx && cy == ny && facingWall(next)) {
			return false;
		}
		
		//checks if next move is only a rotation and no step
		if(cx == nx && cy == ny && ch != nh) {
			return false;
		}
		//checks if next move is one rotation and then a step in that direction
		if(ch != nh) {
			if(nh == 0 && cx == (nx + 1) && cy == ny) {
				return true;
			}
			if(nh == 1 && cx == nx && cy == (ny - 1)){
				return true;
			}
			if(nh == 2 && cx == (nx - 1) && cy == ny){
				return true;
			}
			if(nh == 3 && cx == nx && cy == (ny + 1)){
				return true;
			}
			return false;
		}
		
		
		//returns true if next move is one step away and has same rotation
			if(ch == 0 && cx == (nx + 1) && cy == ny){
				return true;
			}
			if(ch == 1 && cx == nx && cy == (ny - 1)){
				return true;
			}
			if(ch == 2 && cx == (nx - 1) && cy == ny){
				return true;
			}
			if(ch == 3 && cx == nx && cy == (ny + 1)){
				return true;
			}
			return false;
	}

	
	private int[] stateInfo(int StateNumber){
		int[] coord = new int[3]; //index 0 = row-coord, 1 = col-coord, 2 = dir
		coord[0] = (StateNumber-1)/(cols*head);
		coord[1] = ((StateNumber-1)/head) % cols;
		coord[2] = (StateNumber-1) % head;
		return coord;
	}

	
	private void fillObsDiagonals() {
		double[] nothingDiagonal = new double[states];
		for(int i = 0; i < (rows*cols); i++) {
			int n_Ls = 0;
			int n_Ls2 = 0;
			double[] temp = new double[states];
			for(int j = 0; j < (rows*cols); j++) {
				if(i==j) {		//if true location
					for(int h = 0; h<head ; h++) {
						temp[(j*4)+h] = 0.1;
						//System.out.println(temp[(j*4)+h] + "index: " + ((j*4)+h));
					}
				}
				else if(surField(i,j)) {
					n_Ls++;
					for(int h = 0; h < head ; h++) {
						temp[(j*4)+h] = 0.05;
					}
				}
				else if(secSurField(i,j)) {
					n_Ls2++;
					for(int h = 0; h < head ; h++) {
						temp[(j*4)+h] = 0.025;
					}
				}
				else {
					for(int h = 0; h < head ; h++) {
						temp[(j*4)+h] = 0.0;
					}
				}
			}
			obsDiagonals.add(temp);
			double nothingPb = 1.0 - 0.1 - n_Ls*0.05 - n_Ls2*0.025;
			//System.out.println(i + ": nbr sur: " + n_Ls + ", nbr Sec sur:" + n_Ls2);
			for(int h = 0; h < head ; h++) {
				nothingDiagonal[(i*4)+h] = nothingPb;
			}
			
		}
		obsDiagonals.add(nothingDiagonal);
	}
	
	
	private boolean surField(int i, int j) {
		//check down, up, right and left
		if(j == (i+cols) || j == (i-cols) || (j == (i+1) && (i%cols) != (cols-1))|| (j == (i-1) && (i%cols) != 0)) {
			return true;
		}
		//check diagonal
		if((j==(i-cols+1)  && (i%cols) != (cols-1)) 
				|| (j == (i-cols-1) && (i%cols) != 0) 
				|| (j == (i+cols+1)  && (i%cols) != (cols-1))
				|| (j == (i+cols-1) && (i%cols) != 0)) {
			return true;
		}
		return false;
	}
	
	
	private boolean secSurField(int i, int j) {
		//checks straight up, down, left and right
		if(j == (i+2*cols) || j == (i-2*cols) || (j == (i+2) && (i%cols) < (cols-2))|| (j == (i-2) && (i%cols) > 1)) {
			return true;
		}
		//checks top left corner
		if(j==(i-2*cols-2)  && (i%cols) > 1){
			return true;
		}
		//top right corner
		if(j==(i-2*cols+2)  && (i%cols) < (cols-2)){
			return true;
		}
		//bottom left corner
		if(j==(i+2*cols-2)  && (i%cols) > 1){
			return true;
		}
		//bottom right corner
		if(j==(i+2*cols+2)  && (i%cols) < (cols-2)){
			return true;
		}
		
		//checks remaining two fields in top row
		if((j==(i-2*cols-1)  && (i%cols) > 0) || (j==(i-2*cols+1)  && (i%cols) < (cols-1))){
			return true;
		}
		//checks remaining two fields in bottom row
		if((j==(i+2*cols-1)  && (i%cols) > 0) || (j==(i+2*cols+1)  && (i%cols) < (cols-1))){
			return true;
		}
		//checks remaining two fields in left row
		if((j==(i-cols-2)  && (i%cols) > 1) || (j==(i+cols-2)  && (i%cols) > 1)){
			return true;
		}
		//checks remaining two fields in right row
		if((j==(i-cols+2)  && (i%cols) < (cols-2)) || (j==(i+cols+2)  && (i%cols) < (cols-2))){
			return true;
		}
		return false;
	}

	private ArrayList<int[]> getSurFields(int row, int col) {
		int index = row*cols + col;
		ArrayList<int[]> sfList = new ArrayList<int[]>();		
		double[] diagonal = obsDiagonals.get(index);
		for(int i = 0; i < diagonal.length/4; i++) {
			if(diagonal[i*4] == 0.05) {
				int[] coords = new int[2];
				coords[0] = stateInfo((i*4)+1)[0];
				coords[1] = stateInfo((i*4)+1)[1];
				sfList.add(coords);
			}
		}
		return sfList;
	}
	
	private ArrayList<int[]> getSecSurFields(int row, int col) { 
		int index = row*cols + col;
		ArrayList<int[]> sfList = new ArrayList<int[]>();		
		double[] diagonal = obsDiagonals.get(index);
		for(int i = 0; i < diagonal.length/4; i++) {
			if(diagonal[i*4] == 0.025) {
				int[] coords = new int[2];
				coords[0] = stateInfo((i*4)+1)[0];
				coords[1] = stateInfo((i*4)+1)[1];
				sfList.add(coords);
			}
		}
		return sfList;
	}

	private double[][] matrixMul(double[][] left, double[][] right, int leftRows, int leftCols, int rightRows, int rightCols){
		double[][] result = new double[leftRows][rightCols];	
		
		for(int i = 0; i < leftRows; i++) {
			for(int j = 0; j < rightCols; j++) {
				double value = 0;
				for(int k = 0; k < leftCols; k++) {
					value += left[i][k] * right[k][j];
					//System.out.println("left: " + left[i][k] + ", i: " + i + ", k: " + k);
					//System.out.println("right: " + right[k][j] + ", k: " + k + ", j: " + j);
					//System.out.println(value);
				}
				//System.out.println("---");
				result[i][j] = value;
			}
		}
		return result;
	}
	
	private void testMatrixMul() {
		double[][] m1 = new double[3][2];
		m1[0][0] = 1.0;
		m1[0][1] = 2.0;
		//m1[0][2] = 3.0;
		m1[1][0] = 4.0;
		m1[1][1] = 5.0;
		//m1[1][2] = 6.0;
		m1[2][0] = 7.0;
		m1[2][1] = 8.0;
		//m1[2][2] = 9.0;
		
		double[][] m2 = new double[3][1];
		m2[0][0] = 11.0;
		m2[1][0] = 12.0;
		m2[2][0] = 13.0;
//		m2[1][0] = 14.0;
//		m2[1][1] = 15.0;
//		m2[1][2] = 16.0;
//		m2[2][0] = 17.0;
//		m2[2][1] = 18.0;
//		m2[2][2] = 19.0;
		
		double[] v = {1,2,3,4,5,6,7,9};
		
//		double[][] r = matrixMul(m1,m2,1,3,3,1);
		//double[][] r = getTranspose(m1);
		double[][] r = array2diagonalMatrix(v);
//		System.out.println(m1.length);
		for(int i = 0;i<r.length;i++) {
			for(int j = 0;j<r[0].length;j++) {
				System.out.print(r[i][j]);
				System.out.print(",");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	private double[][] getTranspose(double[][] matrix){
		double[][] result = new double[matrix.length][matrix.length];
	       for (int row = 0; row < matrix.length; row++) {
	            for (int col = 0; col < matrix.length; col++) {
	                result[row][col] = matrix[col][row];
		}
	}
	 return result;	
	}
	
	private double[][] array2diagonalMatrix(double[] d){
		double[][] result = new double[d.length][d.length];
		for(int i = 0; i < d.length; i++){
			for(int j = 0; j < d.length; j++){
			result[i][j] = 0;
		}
			result[i][i] = d[i];
		}
		return result;
	}
	
	private double[][] normalize(double[][] matrix) {
		double[][] result = new double[matrix.length][1];
		double sum = 0.0;
		for(int i = 0; i < matrix.length; i++){
			sum += matrix[i][0];
		}
		for(int i = 0; i < matrix.length; i++){
			result[i][0] = (matrix[i][0]/sum);
		}
		
		return result;
	}

	private void calculateF() {
		//System.out.println(currentReading[0] + ", " + currentReading[1]);
		
		double[][] firstMul = matrixMul(tTranspose, fVector, tTranspose.length, tTranspose[0].length, fVector.length, fVector[0].length);
		int index = currentReading[0]*cols + currentReading[1];
		double[][] oMatrix = new double[states][states];
		if(currentReading[0] < 0) {
			oMatrix = array2diagonalMatrix(obsDiagonals.get(rows*cols));
		}
		
		else {
			oMatrix = array2diagonalMatrix(obsDiagonals.get(index));
		}
		double[][] secMul = matrixMul(oMatrix, firstMul, oMatrix.length, oMatrix[0].length, firstMul.length, firstMul[0].length);
		
		double[][] newF = normalize(secMul);
		fVector = newF;
		
		
		for(int i = 0; i < rows*cols;i++) {
			double sum = 0.0;
	
			for(int j = 0; j < head;j++) {
				sum+= fVector[(i*4)+j][0];
			}
			adjFvector[i] = sum;
		}
		double max = 0.0;
		int maxIndex = 0;
		for(int i = 0; i < rows*cols;i++) {
			if(adjFvector[i] > max) {
				maxIndex = i;
				max = adjFvector[i];
			}
		}
		estimatePos[0] = maxIndex / cols;
		estimatePos[1] = maxIndex % cols;
		
	}
	
	private void updateReading() {
		int[] ret = new int[2];
		int rand = new Random().nextInt(1000);
		ArrayList<int[]> n_Ls = getSurFields(trueLocation[0], trueLocation[1]);
		ArrayList<int[]> n_Ls2 = getSecSurFields(trueLocation[0], trueLocation[1]);
		if(rand < 100) {
			ret = trueLocation;
		}
		else if(rand >= 100 && rand < (100 + n_Ls.size()*50)) {
			int rand1 = new Random().nextInt(n_Ls.size());
			ret = n_Ls.get(rand1);
		}
		else if(rand >= (100 + n_Ls.size()*50) && rand < ((100 + n_Ls.size()*50) + n_Ls2.size()*25)) {
			int rand2 = new Random().nextInt(n_Ls2.size());
			ret = n_Ls2.get(rand2);
		}
		else {
			ret[0] = -1;
			ret[1] = -1;
		}
		//System.out.println(ret[0] + ", " + ret[1]);
		currentReading = ret;
	}
	
	private double distance(int[] truePos, int[] estimatePos) {
		double xDist = Math.abs(truePos[0] - estimatePos[0]);
		double yDist = Math.abs(truePos[1] - estimatePos[1]);
			
		
		
		return xDist + yDist;
	}
	
}

	

	
	